using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MergeSort
{
	class Program
	{
		private static int[][] jaggedList;

		#region MergeSort
		private static int[] MergeArray(int[] leftArr, int[] rightArr)
		{
			int[] mergedArr = new int[leftArr.Length + rightArr.Length];

			int leftIndex = 0;
			int rightIndex = 0;
			int mergedIndex = 0;

			// Traverse both arrays simultaneously and store the smallest element of both to mergedArr
			while (leftIndex < leftArr.Length && rightIndex < rightArr.Length)
			{
				if (leftArr[leftIndex] < rightArr[rightIndex])
				{
					mergedArr[mergedIndex++] = leftArr[leftIndex++];
				}
				else
				{
					mergedArr[mergedIndex++] = rightArr[rightIndex++];
				}
			}

			// If any elements remain in the left array, append them to mergedArr
			while (leftIndex < leftArr.Length)
			{
				mergedArr[mergedIndex++] = leftArr[leftIndex++];
			}

			// If any elements remain in the right array, append them to mergedArr
			while (rightIndex < rightArr.Length)
			{
				mergedArr[mergedIndex++] = rightArr[rightIndex++];
			}

			return mergedArr;
		}
		public static int[] ThreadedMergeSort(int[] input, int threads)
		{
			jaggedList = new int[threads][];

			DateTime mergeSortTime = DateTime.Now;
			for (int i = 0; i < threads; i++)
			{

				jaggedList[i] = new int[(input.Length / threads)];

				for (int j = 0; j < input.Length / threads; j++)
				{
					jaggedList[i][j] = input[i * (input.Length / threads) + j];

				}


			}

			Parallel.For(0, threads, i =>
			{
				jaggedList[i] = MergeSort(jaggedList[i], 0, jaggedList[i].Length - 1);

			});
			TimeSpan mergeSortTimeSpan = DateTime.Now - mergeSortTime;
			
			Console.WriteLine("MS SortTime: [" + mergeSortTimeSpan.TotalSeconds.ToString() + "]");
			DateTime UniteTime = DateTime.Now;
			while (threads >= 2)
			{
				for (int i = 0; i < threads / 2; i++)
				{
					jaggedList[i] = MergeArray(jaggedList[(i * 2)], jaggedList[(i * 2) + 1]);
					/*jaggedList[i] = MergeSort(jaggedList[i], 0, jaggedList[i].Length -1);
					jaggedList[i] = jaggedList[i * 2].Concat(jaggedList[(i * 2) + 1]).ToArray();*/

				};

				threads /= 2;

			}
			TimeSpan mergeSortUniteTimeSpan = DateTime.Now - UniteTime;
			Console.WriteLine("MS UniteTime: [" + mergeSortUniteTimeSpan.TotalSeconds.ToString() + "]");
			return jaggedList[0];
		}
		public static int[] MergeSort(int [] input, int start, int end)
		{

			if (start < end)
			{
				
				int middle = (end + start) / 2;				
				int[] leftArr = MergeSort(input, start, middle);
				//Console.WriteLine("���  middle" + start.ToString() + " end " + end.ToString());
				int[] rightArr = MergeSort(input, middle + 1, end);
				int[] mergedArr = MergeArray(leftArr, rightArr);
				return mergedArr;
			}

			//Console.WriteLine("WTF start " + start.ToString() + " end " + end.ToString());
			return new int[] { input[start] };
		}
		#endregion

		#region QuickSort
		public static int[] QuickSort(int[] A, int left, int right)
		{
			if (left > right || left < 0 || right < 0)
			{
			//	Console.WriteLine("ERRRRRRROOROROROROORRRR");
				return null;
			}
			int index = Partition(A, left, right);

			if (index != -1)
			{
				QuickSort(A, left, index - 1);
				QuickSort(A, index + 1, right);
			}
			return A;
		}

		private static int Partition(int[] A, int left, int right)
		{
			if (left > right) return -1;

			int end = left;

			int pivot = A[right];
			for (int i = left; i < right; i++)
			{
				if (A[i] < pivot)
				{
					Swap(A, i, end);
					end++;
				}
			}

			Swap(A, end, right);

			return end;
		}

		private static void Swap(int[] A, int left, int right)
		{
			int tmp = A[left];
			A[left] = A[right];
			A[right] = tmp;
		}

		private static int[] ThreadedQuicksort(int[] array, int threads)
		{
			jaggedList = new int[threads][];

			DateTime quickSortTime = DateTime.Now;
			for (int i = 0; i < threads; i++)
			{

				jaggedList[i] = new int[(array.Length / threads)];

				for (int j = 0; j < array.Length / threads; j++)
				{
					jaggedList[i][j] = array[i * (array.Length / threads) + j];
				}
			}
			

			Parallel.For(0, threads, i =>
			{
				jaggedList[i] = QuickSort(jaggedList[i], 0, jaggedList[i].Length - 1);

			});

			TimeSpan quickSortTimeSpan = DateTime.Now - quickSortTime;
			
			Console.WriteLine("QS SortTime: [" + quickSortTimeSpan.TotalSeconds.ToString() + "]");
			DateTime UniteTime = DateTime.Now;

			while (threads >= 2)
			{
				for (int i = 0; i < threads / 2; i++)
				{

					//jaggedList[i] = QuickSort(jaggedList[i], 0, jaggedList[i].Length - 1);
					jaggedList[i] = MergeArray(jaggedList[(i * 2)], jaggedList[(i * 2) + 1]);

					//jaggedList[i] = jaggedList[i * 2].Concat(jaggedList[(i * 2) + 1]).ToArray();

				};

				threads /= 2;
			}
			TimeSpan quickSortUniteTimeSpan = DateTime.Now - UniteTime;
			Console.WriteLine("QS UniteTime: [" + quickSortUniteTimeSpan.TotalSeconds.ToString() + "]");
			return jaggedList[0];
		}
		static void GenerateInputFile(int size)
		{
			int[] array;
			StreamWriter sw;

			FileStream fileStream;
			switch (size)
			{
				case 25000:
					fileStream = File.Open(@"C:\SSD FAST FOLDER\MergeSort\MergeSort\MergeSort\bin\Debug\25000.txt", FileMode.Open);
					fileStream.SetLength(0);
					fileStream.Close();
					sw = new StreamWriter(@"C:\SSD FAST FOLDER\MergeSort\MergeSort\MergeSort\bin\Debug\25000.txt");
					array = Support.Generator(-1000000, 1000000, size);
					foreach ( int a in array)
						{ sw.WriteLine(a.ToString()); }
					break;
				case 50000:
					fileStream = File.Open(@"C:\SSD FAST FOLDER\MergeSort\MergeSort\MergeSort\bin\Debug\50000.txt", FileMode.Open);
					fileStream.SetLength(0);
					fileStream.Close();
					sw = new StreamWriter(@"C:\SSD FAST FOLDER\MergeSort\MergeSort\MergeSort\bin\Debug\50000.txt");
					array = Support.Generator(-1000000, 1000000, size);
					foreach (int a in array)
					{ sw.WriteLine(a.ToString()); }
					break;
				case 100000:
					fileStream = File.Open(@"C:\SSD FAST FOLDER\MergeSort\MergeSort\MergeSort\bin\Debug\100000.txt", FileMode.Open);
					fileStream.SetLength(0);
					fileStream.Close();
					sw = new StreamWriter(@"C:\SSD FAST FOLDER\MergeSort\MergeSort\MergeSort\bin\Debug\100000.txt");
					array = Support.Generator(-1000000, 1000000, size);
					foreach (int a in array)
					{ sw.WriteLine(a.ToString()); }
					break;
				default:
					sw = new StreamWriter(@"nothing");
					break;
			}

			sw.Close();
		}
		static int[] ReadInputFile(int size)
		{
			StreamReader sr;
			List<int> listInt = new List<int>();
			string line;
			string path = System.Reflection.Assembly.GetEntryAssembly().Location;
			path = path.Remove(path.Length - 13, 13);
			switch (size)
			{
				case 25000:
					//sr = new StreamReader(@"C:\SSD FAST FOLDER\MergeSort\MergeSort\MergeSort\bin\Debug\25000.txt");
					sr = new StreamReader(path + @"25000.txt");
					
					break;
				case 50000:		
					sr = new StreamReader(path + @"50000.txt");
					break;
				case 100000:
					sr = new StreamReader(path + @"100000.txt");
					break;
				default:
					sr = new StreamReader(@"nothing");
					break;
			}
			int kek = 0;
			line = sr.ReadLine();
			while (line != null)
			{
				Int32.TryParse(sr.ReadLine(), out kek);
				listInt.Add(kek);
				line = sr.ReadLine();
			}

			//close the file
			sr.Close();
			return (listInt.ToArray());
		}

		#endregion
		static void Main(string[] args)
		{
			int ArraySize = 0;
			int threads = 1;
			int doQuick = 0;// 0 means not to do
			int doMerge = 0;// 1 means do
			int lastCommand = 0;
			int print = 0;
			int[] array;

			Int32.TryParse(args[0], out ArraySize);//parce array size
			Int32.TryParse(args[1], out threads);//parce num of threads
			Int32.TryParse(args[3], out doQuick);
			Int32.TryParse(args[2], out doMerge);
			if (args.Length == 5)
				Int32.TryParse(args[4], out lastCommand);// if = 1 conmmand line will stop
			if (args.Length == 6)
				Int32.TryParse(args[5], out print);// if = 1 conmmand line will stop
												   //Console.WriteLine("Number of threads: " + threads.ToString() + "Array size: " + ArraySize.ToString());			
												   //GenerateInputFile(25000);
												   //GenerateInputFile(50000);
												   //GenerateInputFile(100000);
												   //array = Support.Generator(-10000, 10000, ArraySize);
			array = ReadInputFile(ArraySize);
			if (print == 1)
			{
				Console.WriteLine("Debug print unsorted array:");
				foreach (int a in array)
					Console.WriteLine(a);
			}
			if (doMerge == 1)
			{
				//Console.WriteLine("--------------------------------------------------");
				DateTime mergeSortTime = DateTime.Now;
				if (threads == 1)
				{
					array = MergeSort(array, 0, array.Length - 1);

				}
				else if (/*array.Length % threads == 0*/true)
				{
					array = ThreadedMergeSort(array, threads);
				}
				TimeSpan mergeSortTimeSpan = DateTime.Now - mergeSortTime;
				Console.WriteLine("MS Threads: [" + threads + "] Time: ["+   mergeSortTimeSpan.TotalSeconds.ToString() + "]");
				//Console.WriteLine("Elapsed Time for Merge sort method: [" + mergeSortTimeSpan.TotalSeconds.ToString() + "]; Threads: [" + threads + "]; Array size: [" + ArraySize + "]");
			}			

			if (doQuick == 1)
			{
				if (doMerge == 1)
					array = Support.Generator(-10000, 10000, ArraySize);
				//Console.WriteLine("--------------------------------------------------");
				//Console.WriteLine("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
				DateTime quicSortTime = DateTime.Now;
				if (threads == 1)
				{
					array = QuickSort(array, 0, array.Length - 1);
				}
				else if (/*array.Length % threads == 0*/ true)
				{
					array = ThreadedQuicksort(array, threads);
				}
				TimeSpan QuickTimeSpan = DateTime.Now - quicSortTime;
				Console.WriteLine("QS Threads: [" + threads + "] Time: [" + QuickTimeSpan.TotalSeconds.ToString() + "]");
				//Console.WriteLine("Elapsed Time for Quick sort method: [" + QuickTimeSpan.TotalSeconds.ToString() + "]; Threads: [" + threads + "]; Array size: [" + ArraySize + "]");
			}
			//test
			bool sorted = true;
			for (int i = 0; i < array.Length-1; i++)
			{				
				if (array[i] > array[i + 1])
					sorted = false;				
			}
			if (!sorted)
				Console.WriteLine("************************************NOT SORTED***********************************************");
			//Console.WriteLine("#########################################SORTED#########################################");
			if (print == 1)
			{
				Console.WriteLine("Debug print sorted array:");
				foreach (int a in array)
					Console.WriteLine(a);
			}
			Console.WriteLine("--------------------------------------------------");
			if (lastCommand == 1)
				Console.ReadKey();
		}



	}
}
