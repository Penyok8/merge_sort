﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MergeSort
{
	class Support
	{
		private static Random random = new Random();
		public static int ReadInt32(string value)
		{
			int val = -1;
			if (!int.TryParse(value, out val))
				return -1;
			return val;
		}

		public static int Randomint(int min, int max)
		{
			return random.Next(min, max);
		}

		public static int[] Generator(int min, int max, int count)
		{
			List<int> array = new List<int>();
			for (int i = 0; i < count; i++)
			{
				array.Add(Randomint(min, max));
			}
			return array.ToArray();
		}
	}
}
